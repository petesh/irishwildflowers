package com.example.simplepage;

import java.util.ArrayList;

public class Plants {
	public static Plant[] allPlants;
	
    public static Plant[] filterBy(
    		String flower_shape,
    		String flower_color,
    		String leaf_type,
    		String plant_height, int requiredMatches) {
    	
    	if (allPlants == null) initAllPlants();
    	
    	Plant.ShapeFlower shape = Plant.shapeFromString(flower_shape);
    	Plant.ColorFlower color = Plant.colorFromString(flower_color);
    	Plant.LeafType leaftype = Plant.leafFromString(leaf_type);
    	Plant.HeightPlant height = Plant.heightFromString(plant_height);
    	
    	ArrayList<Plant> list = new ArrayList<Plant>();
    	for (int i = 0; i < allPlants.length; i++) {
    	    int matchCount = 0;
    		if (allPlants[i].flower_shape == shape || shape == Plant.ShapeFlower.UNKNOWN) matchCount++;
    		if (allPlants[i].flower_color == color || color == Plant.ColorFlower.UNKNOWN) matchCount++;
    		if (allPlants[i].leaf_type == leaftype || leaftype == Plant.LeafType.UNKNOWN) matchCount++;
    		if (allPlants[i].plant_height == height || height == Plant.HeightPlant.UNKNOWN) matchCount++;
    		if (matchCount >= requiredMatches)
    			list.add(allPlants[i]);
    	}
    	return list.toArray(new Plant[list.size()]);
    }
    
    public static Plant plantByName(String name) {
    	if (allPlants == null) initAllPlants();
    	
    	for (int i = 0; i < allPlants.length; i++)
    		if (allPlants[i].plant_name.equalsIgnoreCase(name))
    			return allPlants[i];
    	return null;
    }
    
    public static void initAllPlants() {
        if (allPlants != null) return;
        ArrayList<Plant> plantlist = new ArrayList<Plant>();
        
    	plantlist.add(new Plant("Meadowsweet", "Rose", "Airgead luachra ", "filipendula ulmaria", "white", "broad", "bunched", "large"));
    	plantlist.add(new Plant("Gorse", "Clover and Pea", "Aiteann gallda ", "ulex europaeus", "yellow", "thorny", "bunched", "large"));
    	plantlist.add(new Plant("Red Clover", "Clover and Pea", "Seamair dhearg ", "trifolium pratense", "Blue-purple", "Small", "bunched", "large"));
    	plantlist.add(new Plant("Birds Foot Tre-foil", "Clover and Pea", "Crobh éin corraigh ", "lotus corniculatus", "yellow", "Small", "open", "small"));
    	plantlist.add(new Plant("Bloody Cranesbill", "Geranium", "Crobh dearg", "geranium sanguineum", "Blue-purple", "broad", "open", "small"));
    	plantlist.add(new Plant("Herb Robert", "Geranium", "Ruithéal rí", "geranium robertianum", "Blue-purple", "Small", "open", "small"));
    	plantlist.add(new Plant("Broad-leaved Willow-herb", "Willow-herb", "Lus na Tríonóide", "epilobium montanum", "Blue-purple", "broad", "open", "medium"));
    	plantlist.add(new Plant("Honeysuckle", "Honeysuckle", "Féithleann", "lonicera", "yellow", "broad", "tubed", "large"));
    	plantlist.add(new Plant("Teasel", "Scabious", "Leadán úcaire", "dipsacus fullonum", "Blue-purple", "thorny", "bunched", "large"));
    	
        allPlants = plantlist.toArray(new Plant[plantlist.size()]);
    }
}
