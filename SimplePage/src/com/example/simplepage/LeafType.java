package com.example.simplepage;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class LeafType extends Activity {
	public static final String LEAF_TYPE = "leaf_type";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (BuildConfig.DEBUG) {
		    Plants.initAllPlants();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void clickCommon(String thingToSayClicked) {
		Intent goforward = new Intent(this, FlowerColor.class);
		goforward.putExtra(LEAF_TYPE, thingToSayClicked);
		startActivity(goforward);
	};
	
	public void smallClick(View theView) {
		// This happens when we click on flat button
		clickCommon("small");
	}
	
	public void spikyClick(View theView) {
		//spiky button
		clickCommon("spiky");
	}	
		
	public void broadClick(View theView){
		//broad button
		clickCommon ("broad");
	}
	public void thornyClick(View theView){
		clickCommon ("thorny");
	}

}
