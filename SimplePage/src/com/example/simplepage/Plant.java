package com.example.simplepage;

import junit.framework.Assert;

public class Plant {
	public static enum LeafType {
		SMALL,
		BROAD,
		SPIKY,
		THORNY,
		UNKNOWN
	};
	
	public static enum ColorFlower {
		WHITE,
		YELLOW,
		PINKRED,
		BLUEPURPLE,
		UNKNOWN
	};
	
	public static enum ShapeFlower {
		OPEN,
		BUNCHED,
		TUBED,
		CLOSED,
		UNKNOWN
	};
	
	public static enum HeightPlant {
		SMALL,
		MEDIUM,
		LARGE,
		UNKNOWN
	};
	
    public String plant_name;
    public String plant_family;
    public String irish_name;
    public String latin_name;
    //public String description;
    
    public LeafType leaf_type;
    public ColorFlower flower_color;
    public ShapeFlower flower_shape;
    public HeightPlant plant_height;
    
    public static LeafType leafFromString(String aString) {
    	if (aString.equalsIgnoreCase("small")) return LeafType.SMALL;
    	else if (aString.equalsIgnoreCase("broad")) return LeafType.BROAD;
    	else if (aString.equalsIgnoreCase("spiky")) return LeafType.SPIKY;
    	else if (aString.equalsIgnoreCase("thorny")) return LeafType.THORNY;
    	else return LeafType.UNKNOWN;
    }
    public static ColorFlower colorFromString(String color) {
    	if (color.equalsIgnoreCase("white")) return ColorFlower.WHITE;
    	else if (color.equalsIgnoreCase("yellow")) return ColorFlower.YELLOW;
    	else if (color.equalsIgnoreCase("pink_red")) return ColorFlower.PINKRED;
    	else if (color.equalsIgnoreCase("blue_purple")) return ColorFlower.BLUEPURPLE;
    	else return ColorFlower.UNKNOWN;
    }
    public static ShapeFlower shapeFromString(String shape) {
    	if (shape.equalsIgnoreCase("open")) return ShapeFlower.OPEN;
    	else if (shape.equalsIgnoreCase("bunched")) return ShapeFlower.BUNCHED;
    	else if (shape.equalsIgnoreCase("tubed")) return ShapeFlower.TUBED;
        else if (shape.equalsIgnoreCase("closed")) return ShapeFlower.CLOSED;
    	else return ShapeFlower.UNKNOWN;
    }
    public static HeightPlant heightFromString(String height) {
    	if (height.equalsIgnoreCase("small")) return HeightPlant.SMALL;
    	else if (height.equalsIgnoreCase("medium")) return HeightPlant.MEDIUM;
        else if (height.equalsIgnoreCase("large")) return HeightPlant.LARGE;
    	else return HeightPlant.UNKNOWN;
    }
    
    public Plant(String name, String family, String irish_name, String latin, /*String descr,*/
    		String flower_color, String leaf_type,
    		String flower_shape, String plant_height) {
    	this.plant_name = name;
    	this.plant_family = family;
    	this.irish_name = irish_name;
    	this.latin_name = latin;
    	//this.description = descr;
    	this.leaf_type = leafFromString(leaf_type);
    	this.flower_color = colorFromString(flower_color);
    	this.flower_shape = shapeFromString(flower_shape);
    	this.plant_height = heightFromString(plant_height);
    	if (BuildConfig.DEBUG && this.latin_name == null) {
    	    Assert.assertFalse(this.leaf_type != LeafType.UNKNOWN);
    	    Assert.assertFalse(this.flower_color != ColorFlower.UNKNOWN);
    	    Assert.assertFalse(this.flower_shape != ShapeFlower.UNKNOWN);
    	    Assert.assertFalse(this.plant_height != HeightPlant.UNKNOWN);
    	}
    }
}
