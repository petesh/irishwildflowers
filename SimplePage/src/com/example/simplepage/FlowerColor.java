package com.example.simplepage;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.v4.app.NavUtils;

public class FlowerColor extends Activity {
	public static final String FLOWER_COLOR = "flower_color";
	
	String leaf_type;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_flower_color);
		// Show the Up button in the action bar.
		leaf_type = getIntent().getStringExtra(LeafType.LEAF_TYPE);
		setupActionBar();
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		//getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.flower_color, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
			private void clickCommon(String thingToSayClicked) {
				Intent goforward = new Intent(this, FlowerShape.class);
				goforward.putExtra(FLOWER_COLOR, thingToSayClicked);
				goforward.putExtra(LeafType.LEAF_TYPE, leaf_type);
				startActivity(goforward);
		}
		
		
		
		public void WhiteClick(View theView) {
			//white button
			clickCommon("white");
		}	
		public void pink_redClick(View theView) {
			clickCommon("pink_red");
		}
        public void yellowClick(View theView){
        	clickCommon("yellow");
        }
        public void blueClick(View theView){
        	clickCommon("blue_purple");
        
        }
     } 
       


