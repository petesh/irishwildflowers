package com.example.simplepage;

import java.io.IOException;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

public class PlantDetails extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_plant_details);
		String plant_name = getIntent().getStringExtra(PlantsList.PLANT_NAME);
		Plant p = Plants.plantByName(plant_name);
		// Change images, etc
		TextView tv = (TextView)findViewById(R.id.plant_name);
		tv.setText(p.plant_name);
		tv = (TextView)findViewById(R.id.plant_family);
		tv.setText(p.plant_family);
		tv = (TextView)findViewById(R.id.latin_name);
		tv.setText(p.latin_name);
		tv = (TextView)findViewById(R.id.irish_name);
		tv.setText(p.irish_name);
		// image as well
		ImageView iv = (ImageView)findViewById(R.id.plant_image);
		try {
		Drawable d = Drawable.createFromStream(
				getAssets().open(p.plant_name + ".jpg"), null);
		if (null != d) iv.setImageDrawable(d);
		} catch (IOException ex) {
			// oops, could not find image :(
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.plant_details, menu);
		return true;
	}

}
