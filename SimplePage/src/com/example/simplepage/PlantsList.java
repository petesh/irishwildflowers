package com.example.simplepage;

import android.os.Bundle;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;

import java.io.IOException;

class PlantArrayAdapter extends ArrayAdapter<Plant> {
	private final Context context;
	private final Plant[] plants;
	
	public PlantArrayAdapter(Context context, Plant[] plants) {
		super(context, R.layout.activity_plants_list, plants);
		this.context = context;
		this.plants = plants;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = li.inflate(R.layout.list_plant, parent, false);
		TextView tv = (TextView)rowView.findViewById(R.id.plant_name);
		ImageView iv = (ImageView)rowView.findViewById(R.id.plant_image);
		tv.setText(plants[position].plant_name);
		// set image as well...
		try {
		Drawable d = Drawable.createFromStream(
				context.getAssets().open(plants[position].plant_name + ".jpg"), null);
		if (null != d) iv.setImageDrawable(d);
		} catch (IOException ex) {
			// oops, could not find image :(
		}
		return rowView;
	}
};

public class PlantsList extends Activity {
	public static final String PLANT_NAME = "plant_name";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_plants_list);
		// Show the Up button in the action bar.
		setupActionBar();
		Intent i = getIntent();
		String flower_shape = i.getStringExtra(FlowerShape.FLOWER_SHAPE);
		String flower_color = i.getStringExtra(FlowerColor.FLOWER_COLOR);
		String leaf_type = i.getStringExtra(LeafType.LEAF_TYPE);
		String plant_height = i.getStringExtra(PlantHeight.PLANT_HEIGHT);
		Plant[] plants = Plants.filterBy(flower_shape, flower_color, leaf_type, plant_height, 4);
		ListView lv = (ListView)findViewById(R.id.list);
		lv.setAdapter(new PlantArrayAdapter(this, plants));
		final PlantsList me = this;
		
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {
				Plant selectedItem = (Plant)parent.getItemAtPosition(position);
				Intent intent = new Intent(me, PlantDetails.class);
				intent.putExtra(PLANT_NAME, selectedItem.plant_name);
				startActivity(intent);
			}
		});
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.plants_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
